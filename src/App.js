import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Gallery from './Gallery';

class App extends Component {
  constructor() {
    super();

    this.state = {
      profile: [],
    };
  }
  componentDidMount() {
    fetch('http://jsonplaceholder.typicode.com/photos')
      .then(res => res.json())
      .then((p) => {
        this.setState({ profile: p });
        console.log(p)
      });
  }
  render() {
    return (
      <div>
        {

          this.state.profile.slice(0, 100).map((element) => {

            return (
              <Gallery url={element.url} />

            )
          })
        }




      </div>
    );
  }
}

export default App;
